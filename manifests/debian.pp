class php::debian inherits php::base {

  include php::debian::apc

  File[php_ini_config]{
    path => '/etc/php5/apache2/php.ini',
    require => [ Package[php], Package[apache2] ]
  }
  Package[php]{
    name => 'php5',
    notify => Service[apache]
  }
  package { 'libapache2-mod-php5':
    ensure => installed,
    require => Package[php],
  }

  php::package { [ "curl", "pspell" ]:
    mode => direct
  }
}

class php::debian::apc inherits php::apc {
  Package[php-pecl-apc]{
    name => 'php-apc'
  }
}

class php::debian::suhosin inherits php::suhosin::package {
  Package[php-suhosin]{
    name => 'php5-suhosin'
  }
}
