# manifests/extensions/odbc.pp

class php::extensions::odbc {
    php::package { 'odbc':
        mode => 'direct',
    }
}
