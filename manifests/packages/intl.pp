class php::packages::intl {

  $php_intl = $operatingsystem ? {
    debian => 'php5-intl',
    default => 'php-intl'
  }

  package { "$php_intl":
    ensure => installed,
    alias => "php-intl",
    notify => Service['apache'],
    }
}

  
